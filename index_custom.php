<?php

/**
 * Plugin Name:       Ma mediatheque
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Rosa
 * Description : Gérér une mediatheque
 */


include 'taxo.php';
include 'biblitaxo.php';

if (!defined('index_custom')) {
    function index_custom()
    {


        /**
         * Custom Post Type : Bibliotheque
         */
        $labels = array(
            "name"                  => __("Bibliotheque", "enssop"),
            "singular_name"         => __("Bibliotheque", "enssop"),
            "menu_name"             => __("Bibliotheque", "enssop"),
            "all_items"             => __("Tout les livres", "enssop"),
            "add_new"               => __("Ajouter un livre", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau livre", "enssop"),
            "edit_item"             => __("Ajouter un livre", "enssop"),
            "new_item"              => __("Nouveau livre", "enssop"),
            "view_item"             => __("Voir le livre", "enssop"),
            "view_items"            => __("Voir les livres", "enssop"),
            "search_items"          => __("Chercher un livre", "enssop"),
            "not_found"             => __("Pas de livre trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de livre trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour ce livre", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour ce livre", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour ce livre", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour ce livre", "enssop"),
            "archives"              => __("Type de livre", "enssop"),
            "insert_into_item"      => __("Ajouter au livre", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au livre", "enssop"),
            "filter_items_list"     => __("Filtrer la liste de livre", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste de livre", "enssop"),
            "items_list"            => __("Liste de livre", "enssop"),
            "attributes"            => __("Paramètres du livre", "enssop"),
            "name_admin_bar"        => __("livre", "enssop"),
        );

        $args = array(
            "label"     => __('livre', 'enssop'),
            "labels"    => $labels,
            "description"   =>  __('Livres du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "Livres", "with_front" => true),
            "query_var"             => 'Livres',
            "menu_icon"             => "dashicons-book",
            "supports"              => array("title", 'editor', 'thumbnail'),
        );

        register_post_type('Livres', $args);

        /**
         * Custom Post Type : Cinematheque
         */
        $labels1 = array(
            "name"                  => __("Cinematheque", "enssop"),
            "singular_name"         => __("Cinematheque", "enssop"),
            "menu_name"             => __("Cinematheque", "enssop"),
            "all_items"             => __("Tout les cds", "enssop"),
            "add_new"               => __("Ajouter un cds", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau cd", "enssop"),
            "edit_item"             => __("Ajouter un cd", "enssop"),
            "new_item"              => __("Nouveau cd", "enssop"),
            "view_item"             => __("Voir le cd", "enssop"),
            "view_items"            => __("Voir les cds", "enssop"),
            "search_items"          => __("Chercher un cd", "enssop"),
            "not_found"             => __("Pas de cd trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de cd trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour ce cd", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour ce cd", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour ce cd", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour ce cd", "enssop"),
            "archives"              => __("Type de cd", "enssop"),
            "insert_into_item"      => __("Ajouter au cd", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au cd", "enssop"),
            "filter_items_list"     => __("Filtrer la liste de cd", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste de cd", "enssop"),
            "items_list"            => __("Liste de cd", "enssop"),
            "attributes"            => __("Paramètres du cd", "enssop"),
            "name_admin_bar"        => __("cds", "enssop"),
        );

        $args1 = array(
            "label"     => __('cds', 'enssop'),
            "labels"    => $labels1,
            "description"   =>  __('Cds du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "Cds", "with_front" => true),
            "query_var"             => 'Cds',
            "menu_icon"             => "dashicons-format-audio",
            "supports"              => array("title", 'editor', 'thumbnail'),
        );

        register_post_type('Cds', $args1);


        /**
         * Custom Post Type : Mediatheque
         */
        $labels2 = array(
            "name"                  => __("Mediatheque", "enssop"),
            "singular_name"         => __("Mediatheque", "enssop"),
            "menu_name"             => __("Mediatheque", "senssop"),
            "all_items"             => __("Tout les films", "enssop"),
            "add_new"               => __("Ajouter un films", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau film", "enssop"),
            "edit_item"             => __("Ajouter un film", "enssop"),
            "new_item"              => __("Nouveau film", "enssop"),
            "view_item"             => __("Voir le film", "enssop"),
            "view_items"            => __("Voir les films", "enssop"),
            "search_items"          => __("Chercher un film", "enssop"),
            "not_found"             => __("Pas de film trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de film trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour ce film", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour ce film", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour ce film", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour ce film", "enssop"),
            "archives"              => __("Type de film", "enssop"),
            "insert_into_item"      => __("Ajouter au film", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au film", "enssop"),
            "filter_items_list"     => __("Filtrer la liste de film", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste de film", "enssop"),
            "items_list"            => __("Liste de film", "enssop"),
            "attributes"            => __("Paramètres du film", "enssop"),
            "name_admin_bar"        => __("film", "enssop"),
        );

        $args2 = array(
            "label"     => __('film', 'enssop'),
            "labels"    => $labels2,
            "description"   =>  __('Films du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "Films", "with_front" => true),
            "query_var"             => 'film',
            "menu_icon"             => "dashicons-video-alt2",
            "supports"              => array("title", 'editor', 'thumbnail'),
        );

        register_post_type('Films', $args2);

        /**
         * Custom Post Type : Ludotheque
         */
        $labels3 = array(
            "name"                  => __("Ludotheque", "enssop"),
            "singular_name"         => __("Ludotheque", "enssop"),
            "menu_name"             => __("Ludotheque", "enssop"),
            "all_items"             => __("Tout les jeux", "enssop"),
            "add_new"               => __("Ajouter un jeu", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau jeu", "enssop"),
            "edit_item"             => __("Ajouter un jeu", "enssop"),
            "new_item"              => __("Nouveau jeu", "enssop"),
            "view_item"             => __("Voir le jeu", "enssop"),
            "view_items"            => __("Voir les jeux", "enssop"),
            "search_items"          => __("Chercher un jeux", "enssop"),
            "not_found"             => __("Pas de jeu trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de jeu trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour ce jeu", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour ce jeu", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour ce jeu", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour ce jeu", "enssop"),
            "archives"              => __("Type de jeu", "enssop"),
            "insert_into_item"      => __("Ajouter au jeu", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au jeu", "enssop"),
            "filter_items_list"     => __("Filtrer la liste de jeu", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste de jeu", "enssop"),
            "items_list"            => __("Liste de jeu", "enssop"),
            "attributes"            => __("Paramètres du jeu", "enssop"),
            "name_admin_bar"        => __("jeux", "enssop"),
        );

        $args3 = array(
            "label"     => __('jeux', 'enssop'),
            "labels"    => $labels3,
            "description"   =>  __('Jeux du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "Jeux", "with_front" => true),
            "query_var"             => 'Jeux',
            "menu_icon"             => "dashicons-desktop",
            "supports"              => array("title", 'editor', 'thumbnail'),
        );

        register_post_type('Jeux', $args3);

        /**
         * Custom Post Type : Evenements
         */
        $labels4 = array(
            "name"                  => __("Evenement", "enssop"),
            "singular_name"         => __("Evenement", "enssop"),
            "menu_name"             => __("Evenement", "enssop"),
            "all_items"             => __("Tout les evenements", "enssop"),
            "add_new"               => __("Ajouter un evenement", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau evenement", "enssop"),
            "edit_item"             => __("Ajouter un evenement", "enssop"),
            "new_item"              => __("Nouveau evenement", "enssop"),
            "view_item"             => __("Voir le evenement", "enssop"),
            "view_items"            => __("Voir les evenements", "enssop"),
            "search_items"          => __("Chercher un evenement", "enssop"),
            "not_found"             => __("Pas de evenement trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de evenement trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour ce evenement", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour ce evenement", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour ce evenement", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour ce evenement", "enssop"),
            "archives"              => __("Type d evenement", "enssop"),
            "insert_into_item"      => __("Ajouter au evenement", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au evenement", "enssop"),
            "filter_items_list"     => __("Filtrer la liste de evenement", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste de evenement", "enssop"),
            "items_list"            => __("Liste de evenement", "enssop"),
            "attributes"            => __("Paramètres du evenement", "enssop"),
            "name_admin_bar"        => __("evenement", "enssop"),
        );

        $args4 = array(
            "label"     => __('evenement', 'enssop'),
            "labels"    => $labels4,
            "description"   =>  __('Evenement du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  3,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "Evenement", "with_front" => true),
            "query_var"             => 'Evenement',
            "menu_icon"             => "dashicons-buddicons-groups",
            "supports"              => array("title", 'editor', 'thumbnail'),
        );

        register_post_type('Evenement', $args4);
    }
}

add_action('init', 'index_custom');
