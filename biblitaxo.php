<?php

if (!defined('biblitaxo')) {
    function biblitaxo()
    {

        /**
         * Taxonomy :Bibliotheque
         */

        $labels = array(
            'name' => __('Bibliotheque', 'enssop'),
            'singular_name' => __('Type de bibliotheque', 'enssop'),
            'search_items' =>  __('Rechercher un type de bibliotheque', 'enssop'),
            'all_items' => __('Tous les types de bibliotheque', 'enssop'),
            'parent_item' => __('Type de bibliotheque Parent', 'enssop'),
            'parent_item_colon' => __('Type de bibliotheque Parent :', 'enssop'),
            'edit_item' => __('Modifier le type   d bibliotheque', 'enssop'),
            'update_item' => __('Modifier le type d bibliotheque', 'enssop'),
            'add_new_item' => __('Ajouter un type d bibliotheque', 'enssop'),
            'new_item_name' => __('Nouveau type   d bibliotheque', 'enssop'),
            'menu_name' => __('types d bibliotheque'),
        );

        $args = array(
            'hierarchical'    => true,
            'labels'  => $labels,
            'show_ui' => true,
            'show_admin_column'   =>  true,
            'query_var'   => true,
            'rewrite' => array('slug'    =>  'bibliotheque-type'),
        );
        register_taxonomy('type', array('bibliotheque'), $args);


        /**
         * Taxonomy: livres
         */

        $labels = array(
            "name"          => __("Type livre", "ensssop"),
            "singular_name" => __("Type livre", "ensssop"),
            "menu_name"     => __("Type livre", "ensssop"),
            "all_items"     => __("Tous les types livres", "ensssop"),
            "edit_item"     => __("Modifier le type livres", "ensssop"),
            "view_item"     => __("Voir le livre", "ensssop"),
        );

        $args = array(
            "label"                 => __("type livre", "enssop"),
            "labels"                => $labels,
            "public"                => true,
            "publicly_queryable"    => true,
            "hierarchical"          => true,
            "show_ui"               => true,
            "show_in_menu"          => true,
            "show_in_nav_menus"     => true,
            "query_var"             => true,
            "rewrite"               => array('slug' => 'type livre', 'with_front' => true,),
            "show_admin_column"     => false,
            "show_in_rest"          => true,
            "rest_base"             => "type-de-livre",
            "rest_controller_class" => "WP_REST_Terms_Controller",
            "show_in_quick_edit"    => false,
        );
        register_taxonomy("livre", array("bibliotheque"), $args);
    }
}

add_action('init', 'taxo');
