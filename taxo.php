<?php

if (!defined('taxo')) {
    function taxo()
    {
       
        /**
         * Taxonomy :Evenement
         */

        $labels = array(
            'name' => __('Evenement', 'enssop'),
            'singular_name' => __('Type d evenement', 'enssop'),
            'search_items' =>  __('Rechercher un type d evenement', 'enssop'),
            'all_items' => __('Tous les types d evenement', 'enssop'),
            'parent_item' => __('Type d evenement Parent', 'enssop'),
            'parent_item_colon' => __('Type d evenement Parent :', 'enssop'),
            'edit_item' => __('Modifier le type d evenement', 'enssop'),
            'update_item' => __('Modifier le type d evenement', 'enssop'),
            'add_new_item' => __('Ajouter un type d evenement', 'enssop'),
            'new_item_name' => __('Nouveau type   d evenement', 'enssop'),
            'menu_name' => __('types d evenement'),
        );

        $args = array(
            'hierarchical'    => true,
            'labels'  => $labels,
            'show_ui' => true,
            'show_admin_column'   =>  true,
            'query_var'   => true,
            'rewrite' => array('slug'    =>  'evenement-type'),
        );
        register_taxonomy('type', array('evenement'), $args);


        /**
         * Taxonomy: type evenement
         */

        $labels = array(
            "name"          => __("Type evenement", "ensssop"),
            "singular_name" => __("Type evenement", "ensssop"),
            "menu_name"     => __("Type evenement", "ensssop"),
            "all_items"     => __("Tous les types evenements", "ensssop"),
            "edit_item"     => __("Modifier le type evenement", "ensssop"),
            "view_item"     => __("Voir l evenement", "ensssop"),
        );

        $args = array(
            "label"                 => __("type evenement", "enssop"),
            "labels"                => $labels,
            "public"                => true,
            "publicly_queryable"    => true,
            "hierarchical"          => true,
            "show_ui"               => true,
            "show_in_menu"          => true,
            "show_in_nav_menus"     => true,
            "query_var"             => true,
            "rewrite"               => array('slug' => 'type evenement', 'with_front' => true,),
            "show_admin_column"     => false,
            "show_in_rest"          => true,
            "rest_base"             => "type-de-evenement",
            "rest_controller_class" => "WP_REST_Terms_Controller",
            "show_in_quick_edit"    => false,
        );
        register_taxonomy("type evenement", array("Evenement"), $args);



       }
}

add_action('init', 'taxo');
